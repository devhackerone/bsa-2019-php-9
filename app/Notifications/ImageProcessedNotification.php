<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Entities\Photo;
use Illuminate\Support\Facades\Storage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class ImageProcessedNotification extends Notification
{
    use Queueable;

    public $photo;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'broadcast'];
    }


    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'status' => $this->photo->status(),
            'photo_100_100' => Storage::url($this->photo->photo_100_100),
            'photo_150_150' => Storage::url($this->photo->photo_150_150),
            'photo_250_250' => Storage::url($this->photo->photo_250_250),
            'timestamp' => $this->photo->timestamp(),
        ]);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line("Dear {$notifiable->name},")
            ->line("Photos have been successfully uploaded and processed.")
            ->line("Here are links to the images:")
            ->line("")
            ->line(url(Storage::url($this->photo->photo_100_100)) . ' 100x100 photo')
            ->line(url(Storage::url($this->photo->photo_150_150)) . ' 150x150 photo')
            ->line(url(Storage::url($this->photo->photo_250_250)) . ' 250x250 photo')
            ->line("")
            ->line("Thanks!");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
