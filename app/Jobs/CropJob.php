<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\Contracts\PhotoService;
use App\Entities\Photo;
use App\Entities\User;
use App\Notifications\ImageProcessedNotification;
use App\Notifications\ImageProcessingFailedNotification;


class CropJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $photo;
    public $user;
    public $tries = 1;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
        $this->user = User::find($this->photo->user_id);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PhotoService $photoService)
    {
        $this->photo->update([
            'status' => 'PROCESSING'
        ]);

        $cropSizes = [
            'photo_100_100' => ['width' => 100, 'height' => 100],
            'photo_150_150' => ['width' => 150, 'height' => 150],
            'photo_250_250' => ['width' => 250, 'height' => 250]
        ];

        $cropImagePath = [];

        foreach ($cropSizes as $key => $size) {
            $cropImagePath[$key] = $photoService
                ->crop(
                    $this->photo->original_photo, $size['width'], $size['height']
                );

            $this->photo->update([
                "photo_{$size['width']}_{$size['height']}"
                => $cropImagePath[$key]
            ]);
        }

        $this->photo->update([
            'status' => 'SUCCESS'
        ]);

        $this->user->notify(new ImageProcessedNotification($this->photo));
    }

    public function failed()
    {
        $this->photo->update([
            'status' => 'FAIL'
        ]);

        $this->user->notify(new ImageProcessingFailedNotification($this->photo));
    }
}
