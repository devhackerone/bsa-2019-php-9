<?php


namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;


class Photo extends Model
{
    protected $fillable = [
        'user_id',
        'original_photo',
        'photo_100_100',
        'photo_150_150',
        'photo_250_250',
        'status',
    ];

    public function status(): string
    {
        return $this->status;
    }

    public function timestamp(): string
    {
        return Carbon::parse($this->dateTime)->format('Y-m-d\TH:i:s');
    }

    public function getStatusAttribute(String $status): string
    {
        return strtolower($status);
    }
}